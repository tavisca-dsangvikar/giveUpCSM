﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSupplierMapper.Models;

namespace ClientSupplierMapper.Controllers
{
    public class ClientSupplierController : Controller
    {
        private ClientSupplierMapperContextModel db = new ClientSupplierMapperContextModel();

        //
        // GET: /ClientSupplier/

        public ActionResult Index()
        {
            var clientsuppliermapper = db.ClientSupplierMapper.Include(c => c.Supplier).Include(c => c.Client).Include(c => c.Product);
            return View(clientsuppliermapper.ToList());
        }

        //
        // GET: /ClientSupplier/Details/5

        public ActionResult Details(int id = 0)
        {
            ClientSupplierMap clientsuppliermap = db.ClientSupplierMapper.Find(id);
            if (clientsuppliermap == null)
            {
                return HttpNotFound();
            }
            return View(clientsuppliermap);
        }

        //
        // GET: /ClientSupplier/Create

        public ActionResult Create()
        {
            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name");
            ViewBag.ID_Client = new SelectList(db.Client, "ID", "Name");
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type");
            return View();
        }

        //
        // POST: /ClientSupplier/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClientSupplierMap clientsuppliermap)
        {
            if (ModelState.IsValid)
            {
                db.ClientSupplierMapper.Add(clientsuppliermap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name", clientsuppliermap.ID_Supplier);
            ViewBag.ID_Client = new SelectList(db.Client, "ID", "Name", clientsuppliermap.ID_Client);
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type", clientsuppliermap.ID_Product);
            return View(clientsuppliermap);
        }

        //
        // GET: /ClientSupplier/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ClientSupplierMap clientsuppliermap = db.ClientSupplierMapper.Find(id);
            if (clientsuppliermap == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name", clientsuppliermap.ID_Supplier);
            ViewBag.ID_Client = new SelectList(db.Client, "ID", "Name", clientsuppliermap.ID_Client);
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type", clientsuppliermap.ID_Product);
            return View(clientsuppliermap);
        }

        //
        // POST: /ClientSupplier/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClientSupplierMap clientsuppliermap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(clientsuppliermap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name", clientsuppliermap.ID_Supplier);
            ViewBag.ID_Client = new SelectList(db.Client, "ID", "Name", clientsuppliermap.ID_Client);
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type", clientsuppliermap.ID_Product);
            return View(clientsuppliermap);
        }

        //
        // GET: /ClientSupplier/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ClientSupplierMap clientsuppliermap = db.ClientSupplierMapper.Find(id);
            if (clientsuppliermap == null)
            {
                return HttpNotFound();
            }
            return View(clientsuppliermap);
        }

        //
        // POST: /ClientSupplier/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ClientSupplierMap clientsuppliermap = db.ClientSupplierMapper.Find(id);
            db.ClientSupplierMapper.Remove(clientsuppliermap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}