﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientSupplierMapper.Models;

namespace ClientSupplierMapper.Controllers
{
    public class SupplierProductController : Controller
    {
        private ClientSupplierMapperContextModel db = new ClientSupplierMapperContextModel();

        //
        // GET: /SupplierProduct/

        public ActionResult Index()
        {
            var suppliersproductmapper = db.SuppliersProductMapper.Include(s => s.Supplier).Include(s => s.Product);
            return View(suppliersproductmapper.ToList());
        }

        //
        // GET: /SupplierProduct/Details/5

        public ActionResult Details(int id = 0)
        {
            SupplierProductMapper supplierproductmapper = db.SuppliersProductMapper.Find(id);
            if (supplierproductmapper == null)
            {
                return HttpNotFound();
            }
            return View(supplierproductmapper);
        }

        //
        // GET: /SupplierProduct/Create

        public ActionResult Create()
        {
            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name");
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type");
            return View();
        }

        //
        // POST: /SupplierProduct/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SupplierProductMapper supplierproductmapper)
        {
            if (ModelState.IsValid)
            {
                db.SuppliersProductMapper.Add(supplierproductmapper);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name", supplierproductmapper.ID_Supplier);
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type", supplierproductmapper.ID_Product);
            return View(supplierproductmapper);
        }

        //
        // GET: /SupplierProduct/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SupplierProductMapper supplierproductmapper = db.SuppliersProductMapper.Find(id);
            if (supplierproductmapper == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name", supplierproductmapper.ID_Supplier);
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type", supplierproductmapper.ID_Product);
            return View(supplierproductmapper);
        }

        //
        // POST: /SupplierProduct/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SupplierProductMapper supplierproductmapper)
        {
            if (ModelState.IsValid)
            {
                db.Entry(supplierproductmapper).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_Supplier = new SelectList(db.Suppliers, "ID", "Name", supplierproductmapper.ID_Supplier);
            ViewBag.ID_Product = new SelectList(db.Product, "ID", "Type", supplierproductmapper.ID_Product);
            return View(supplierproductmapper);
        }

        //
        // GET: /SupplierProduct/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SupplierProductMapper supplierproductmapper = db.SuppliersProductMapper.Find(id);
            if (supplierproductmapper == null)
            {
                return HttpNotFound();
            }
            return View(supplierproductmapper);
        }

        //
        // POST: /SupplierProduct/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SupplierProductMapper supplierproductmapper = db.SuppliersProductMapper.Find(id);
            db.SuppliersProductMapper.Remove(supplierproductmapper);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}