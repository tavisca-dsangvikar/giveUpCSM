﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ClientSupplierMapper.Models
{
    public class Client
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class Supplier
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class Product
    {
        [Key]
        public int ID { get; set; }
        public string Type { get; set; }
    }

    public class SupplierProductMapper
    {

        [Key, Column(Order = 0)]
        public int ID_Supplier { get; set; }
        [ForeignKey("ID_Supplier")]
        public Supplier Supplier { get; set; }

        [Key, Column(Order = 1)]
        public int ID_Product { get; set; }
        [ForeignKey("ID_Product")]
        public Product Product { get; set; }
    }

    public class ClientSupplierMap
    {
        [Key]
        [Column(Order = 1)]
        public int ID_Supplier { get; set; }
        [ForeignKey("ID_Supplier")]
        public Supplier Supplier { get; set; }

        [Key]
        [Column(Order = 2)]
        public int ID_Client { get; set; }
        [ForeignKey("ID_Client")]
        public Client Client { get; set; }

        [Key]
        [Column(Order = 3)]
        public int ID_Product { get; set; }
        [ForeignKey("ID_Product")]
        public Product Product { get; set; }
    }

    public class ClientSupplierMapperContextModel : DbContext
    {
        public DbSet<Client> Client { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<SupplierProductMapper> SuppliersProductMapper { get; set; }
        public DbSet<ClientSupplierMap> ClientSupplierMapper { get; set; }
       
    }

    public class BigViewModel
    {
        public Client Client { get; set; }
        public Supplier Suppliers { get; set; }
        public Product Product{ get; set; }
        public SupplierProductMapper SuppliersProductMapper { get; set; }
        public ClientSupplierMap ClientSupplierMap { get; set; }
    }
   
    
}